package com.example.samplebleapp;

import android.app.Application;
import android.content.res.Configuration;
import android.content.Context;
import android.util.Log;

import com.polidea.rxandroidble2.RxBleClient;
import com.polidea.rxandroidble2.internal.RxBleLog;

public class BleApplication extends Application {
    private RxBleClient rxBleClient;
    // Called when the application is starting, before any other application objects have been created.
    // Overriding this method is totally optional!
    @Override
    public void onCreate() {
        super.onCreate();
        Log.i("APP", "Log from application");
        // Required initialization logic here!
        rxBleClient = RxBleClient.create(this);
        RxBleClient.setLogLevel(RxBleLog.VERBOSE);
    }

    // Called by the system when the device configuration changes while your component is running.
    // Overriding this method is totally optional!
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    // This is called when the overall system is running low on memory,
    // and would like actively running processes to tighten their belts.
    // Overriding this method is totally optional!
    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    public static RxBleClient getRxBleClient(Context context) {
        /* TODO: Complete singleton behaviour for RxBleClient getter
            Get application from context, cast it to BleApplication type
            Return rxBleClient field
         */
        BleApplication application = (BleApplication) context.getApplicationContext();
        return application.rxBleClient;
    }
}
