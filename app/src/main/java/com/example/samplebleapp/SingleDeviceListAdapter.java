package com.example.samplebleapp;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

class SingleDeviceListAdapter extends RecyclerView.Adapter<SingleDeviceListAdapter.DeviceViewHolder>  {
    private static final String TAG = "BleSample-SingleDeviceListAdapter";
    private IDeviceOperationsListener mListener;

    private ArrayList<String> mData = new ArrayList<>();

    SingleDeviceListAdapter(IDeviceOperationsListener listener) {
        mListener = listener;
    }

    public class DeviceViewHolder extends RecyclerView.ViewHolder {
        TextView mDeviceNameTv;

        Button mToggleLedBtn;

        DeviceViewHolder(View v) {
            super(v);
            mDeviceNameTv = (TextView) v.findViewById(R.id.tvName);
            mToggleLedBtn = (Button) v.findViewById(R.id.btToggleLed);
        }
    }

    @NonNull
    @Override
    public DeviceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.device_item, parent, false);

        return new DeviceViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DeviceViewHolder holder, int position) {
        final String deviceMacAddress = mData.get(position);

        // TODO: Update holder's text view with device address
        holder.mDeviceNameTv.setText(deviceMacAddress);

        if (!holder.mToggleLedBtn.hasOnClickListeners())
        {
            // TODO: Implement onClickListener for holder's button, on click invoke toggle LED command using one of MainActivity methods
            // More information about Button's onClickListener: https://developer.android.com/reference/android/widget/Button
            holder.mToggleLedBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onToggleLed(deviceMacAddress);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    // This method adds new device if it doesn't present in dataset
    void onAdvPacket(BleDevice device) {
        if (!isPresented(device)) {
            mData.add(device.getAddress());
        }
        // TODO: Call one of RecyclerView.Adapter's methods to make RecyclerView redraw view (notify about new state of dataset)
        notifyDataSetChanged();
    }

    void onDeviceListClear() {
        mData = new ArrayList<>();
        notifyDataSetChanged();
    }

    private boolean isPresented(BleDevice device) {
        for (String deviceAddress : mData)
            if (deviceAddress.equals(device.getAddress()))
                return true;
        return false;
    }
}
