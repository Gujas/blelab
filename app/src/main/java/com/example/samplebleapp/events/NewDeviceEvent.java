package com.example.samplebleapp.events;

import com.example.samplebleapp.BleDevice;

public class NewDeviceEvent {
    public final BleDevice device;

    public NewDeviceEvent(BleDevice device) {
        this.device = device;
    }

    public NewDeviceEvent() {
        this.device = null;
    }
}