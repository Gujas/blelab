package com.example.samplebleapp;

public interface IDeviceOperationsListener {
    void onToggleLed(String node);
}
