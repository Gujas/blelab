package com.example.samplebleapp;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import com.polidea.rxandroidble2.RxBleConnection;
import com.polidea.rxandroidble2.RxBleDevice;

import java.util.UUID;

import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class BleDevice {
    private static final String TAG = "BleSample-BLE-DEVICE";

    private static final String UUID_CHAR_LED = "0000ff02-0000-1000-8000-00805f9b34fb";

    private RxBleDevice mRxBleDevice;
    private Disposable mConnectionDisposable;
    private Disposable mConnectionStateDisposable;

    private byte[] mLedModes = new byte[] { 0x0, 0x1};
    private int mCurrentLedMode = 0;
    private RxBleConnection mConnection;

    public BleDevice(RxBleDevice device) {
        mRxBleDevice = device;

        mConnectionStateDisposable = mRxBleDevice.observeConnectionStateChanges()
                .observeOn(Schedulers.io())
                .subscribe(this::onConnectionStateChange);
    }

    private void onConnectionStateChange(RxBleConnection.RxBleConnectionState newState) {
        // When device was disconnected set put null to mConnection to validate that device is disconnected before next connection
        if (newState == RxBleConnection.RxBleConnectionState.DISCONNECTED)
            mConnection = null;
        Log.d(TAG, "onConnectionStateChange: " + newState.toString());
    }

    public String getAddress() {
        return mRxBleDevice.getMacAddress();
    }

    public void disposeDevice() {
        if (mConnectionStateDisposable != null) {
            mConnectionStateDisposable.dispose();
            mConnectionStateDisposable = null;
        }

        if (mConnectionDisposable != null) {
            mConnectionDisposable.dispose();
            mConnectionDisposable = null;
        }
    }
    public void toggleLed() {
        mCurrentLedMode = (mCurrentLedMode == 0 ? 1 : 0);

        // TODO: Implement LED toggler method
        /*
            1) Check that device is disconnected
            2) Establish connection
            3) Read characteristic
            4) Invert characteristic value
            5) Write characteristic
            6) On any result disconnect from device
            7) Report any errors via Logcat or Toast
         */
        mRxBleDevice.establishConnection(false)
                .flatMapSingle(rxBleConnection -> rxBleConnection.writeCharacteristic(UUID.fromString(UUID_CHAR_LED), mLedModes))
                .subscribe(
                        characteristicValue -> {
                            // Characteristic value confirmed.
                        },
                        throwable -> {
                            // Handle an error here.
                        }
                );
    }
}
