package com.example.samplebleapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.example.samplebleapp.events.NewDeviceEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class MainActivity extends AppCompatActivity implements IDeviceOperationsListener, SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = "BleSampleMainActivity";
    private static final int REQUEST_COARSE_LOCATION_PERMISSIONS = 1001;

    private SingleDeviceListAdapter mListAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView mDevicesListView;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        initUI();
        tryRunBleService();
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void tryRunBleService() {
        boolean hasBlePermission = ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        if (hasBlePermission) {
            runBleService();
            return;
        }

        if (!hasBlePermission) {

            Log.d(TAG, "Request sending: " + REQUEST_COARSE_LOCATION_PERMISSIONS);
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{
                            android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_COARSE_LOCATION_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        Log.d(TAG, "Request result: " + requestCode);
        switch (requestCode) {
            case REQUEST_COARSE_LOCATION_PERMISSIONS: {
                if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    tryRunBleService();
                } else {
                    Toast.makeText(this,
                            getResources().getString(R.string.ble_permission_failure),
                            Toast.LENGTH_LONG).show();

                }
                break;
            }
        }
    }

    private void runBleService(){
        /* TODO: Start BleDeviceService service to begin devices discovering */
        startService(new Intent(this, BleDeviceService.class));
    }

    private void initUI() {
        setContentView(R.layout.activity_main);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.srlMainRefresher);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mListAdapter = new SingleDeviceListAdapter(this);
        mDevicesListView = (RecyclerView) findViewById(R.id.lvFoundDevices);
        mLayoutManager = new LinearLayoutManager(this);
        mDevicesListView.setLayoutManager(mLayoutManager);
        mDevicesListView.setAdapter(mListAdapter);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNewDeviceEvent(NewDeviceEvent event) {
        if (event.device != null) {
            // TODO: Pass new device to mListAdapter using one of it's methods
            mListAdapter.onAdvPacket(event.device);
        }
        else {
            mListAdapter.onDeviceListClear();
            // TODO: Disable refreshing indicator for SwipeRefreshLayout, additional info: https://developer.android.com/training/swipe/
            mSwipeRefreshLayout.setEnabled(false);
        }
    }

    public void onToggleLed(String deviceAddress) {
        Intent serviceIntent = new Intent(this, BleDeviceService.class);
        serviceIntent.putExtra(BleDeviceService.SERVICE_MESSAGE_TYPE_NAME, BleDeviceService.SERVICE_MESSAGE_TYPE_VALUE_TOGGLE_LED_ON_OFF);
        serviceIntent.putExtra(BleDeviceService.SERVICE_DEVICE_ADDRESS, deviceAddress);
        startService(serviceIntent);
    }

    @Override
    public void onRefresh() {
        Log.i(TAG, "onRefresh called from SwipeRefreshLayout");
        /* TODO: Implement Swipe-to-Refresh callback
            This callback should send special command to service to clear list of discovered devices
            More info about Swipe-to-Refresh: https://developer.android.com/training/swipe/
        */
        mSwipeRefreshLayout.setRefreshing(true);
       // mSwipeRefreshLayout.setEnabled(true);

    }
}
