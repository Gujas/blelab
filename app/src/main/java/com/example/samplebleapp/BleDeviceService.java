package com.example.samplebleapp;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.example.samplebleapp.events.NewDeviceEvent;
import com.polidea.rxandroidble2.RxBleClient;
import com.polidea.rxandroidble2.RxBleDevice;
import com.polidea.rxandroidble2.exceptions.BleScanException;
import com.polidea.rxandroidble2.scan.ScanResult;
import com.polidea.rxandroidble2.scan.ScanSettings;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;

public class BleDeviceService extends Service {
    public static final String TAG = "BleSampleService";

    private static final String DEVICE_NAME = "BedJet";

    public static final String SERVICE_MESSAGE_TYPE_NAME = "MESSAGE_TYPE";
    public static final int SERVICE_MESSAGE_TYPE_VALUE_REFRESH = 3;
    public static final int SERVICE_MESSAGE_TYPE_VALUE_TOGGLE_LED_ON_OFF = 4;
    public static final String SERVICE_DEVICE_ADDRESS = "BLE_DEVICE_ADDRESS";

    private List<BleDevice> mDevices = new ArrayList<>();

    private RxBleClient mRxBleClient;
    private Disposable mScanDisposable;

    @Override
    public void onCreate() {
        super.onCreate();
        mRxBleClient = BleApplication.getRxBleClient(this);

        initBleScanner();
        initializeNotifications();
    }

    private void initializeNotifications() {
        Notification.Builder builder = new Notification.Builder(this);
        Notification notification;
        notification = builder
                .setContentTitle("BLE Sample")
                .setContentText("Data service is running...")
                .build();
        startForeground(112233, notification);
    }

    private void initBleScanner() {
        mScanDisposable = mRxBleClient.scanBleDevices(
                new ScanSettings.Builder()
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                        .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
                        .build()
                )
                .doFinally(this::dispose)
                .subscribe(
                        this::addScanResult,
                        this::onScanFailure
                );
    }

    void addScanResult(ScanResult bleScanResult) {
        /* TODO: Implement handling of scan results
            1) Get instance of RxBleDevice from scan result
            2) Check if it has correct name (use helper method)
            3) Check if this device has already been discovered
            4) If steps 2 and 3 are true add new instance of BleDevice to discovered devices
         */
        /*
        RxBleDevice device = bleScanResult.getBleDevice();
        if (isCorrectDeviceName(device) && doesContain(device)) {
            addDevice(new BleDevice(device));
        }
         */
    }

    private void dispose() {
        if (mScanDisposable != null && !mScanDisposable.isDisposed()) {
            mScanDisposable.dispose();
            mScanDisposable = null;
        }
    }

    private void onScanFailure(Throwable throwable) {
        if (throwable instanceof BleScanException) {
            Log.w("EXCEPTION", throwable.getMessage());
            Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isCorrectDeviceName(RxBleDevice device) {
        return device.getName() != null &&
                device.getName().equals(DEVICE_NAME);
    }

    /** Check if we've already add the device*/
    private boolean doesContain(RxBleDevice device) {
        for (BleDevice bleDevice : mDevices)
            if (bleDevice.getAddress().equals(device.getMacAddress()))
                return true;

        return false;
    }

    private void addDevice(BleDevice bleDevice) {
        mDevices.add(bleDevice);

        notifyAboutNewBleDevice(bleDevice);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            int messageType = intent.getIntExtra(SERVICE_MESSAGE_TYPE_NAME, -1);
            switch (messageType) {
                case SERVICE_MESSAGE_TYPE_VALUE_REFRESH:
                    Log.d(TAG, "Service. Received command: BJ_SERVICE_MESSAGE_TYPE_VALUE_REFRESH");
                    disconnectDevices();
                    break;

                case SERVICE_MESSAGE_TYPE_VALUE_TOGGLE_LED_ON_OFF: {
                    Log.d(TAG, "Service. Received command: SERVICE_MESSAGE_TYPE_VALUE_TOGGLE_LED_ON_OFF");
                    String targetDeviceAddress = intent.getStringExtra(SERVICE_DEVICE_ADDRESS);
                    for (BleDevice device : mDevices) {
                        if (targetDeviceAddress.equals(device.getAddress())) {
                            device.toggleLed();
                        }
                    }
                    break;
                }

                default:
                    Log.w(TAG, "Service. Received command: UNKNOWN (" + messageType + ")");
            }
        }

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        dispose();
        disconnectDevices();
        super.onDestroy();
    }

    private void disconnectDevices( ){
        for (BleDevice device : mDevices)
            device.disposeDevice();

        mDevices.clear();
        notifyAboutDeviceListClear();
    }

    private void notifyAboutNewBleDevice(BleDevice device) {
        EventBus.getDefault().post(new NewDeviceEvent(device));
    }

    private void notifyAboutDeviceListClear() {
        EventBus.getDefault().post(new NewDeviceEvent());
    }
}
